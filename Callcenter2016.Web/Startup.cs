﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Callcenter2016.Web.Startup))]
namespace Callcenter2016.Web
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
