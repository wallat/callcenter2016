﻿using Callcenter2016.CORE;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Callcenter2016.Web.Controls
{
    public partial class MessageControl : System.Web.UI.UserControl
    {
        public Message Message { get; set; }
              

        protected void Page_Load(object sender, EventArgs e)
        {
            UserDate.Text = Message.Date ==null ? "" :Message.Date.ToString("dd/MM/yyyy");
            AdminDate.Text = Message.Date == null ? "" : Message.Date.ToString("dd/MM/yyyy");

            UserName.Text = Message.User==null ? "" :Message.User.UserName;
            AdminName.Text = Message.User == null ? "" : Message.User.UserName;

            if (File.Exists(Server.MapPath("~/Upload/Profile/")
                           + Message.User_Id + ".jpg"))
            {
                UserImage.ImageUrl = "~/Upload/Profile/"
                            + Message.User_Id + ".jpg";
                UserImage.Visible = true;
                AdminImage.ImageUrl = "~/Upload/Profile/"
                            + Message.User_Id + ".jpg";
                AdminImage.Visible = true;
            }

            Text.Text = Message.Text;

            bool isAdmin = false;
            if (Message.User_Id != null)
            {
                var userManager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                isAdmin = userManager.IsInRoleAsync(Message.User_Id, "Admin").Result;
            }

            User.Visible = !isAdmin;            
            Admin.Visible = isAdmin;

            Text.CssClass = isAdmin ? "bubble bubble-alt" : "bubble";
        }
        
    }
}