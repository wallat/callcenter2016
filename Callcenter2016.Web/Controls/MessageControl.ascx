﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MessageControl.ascx.cs" Inherits="Callcenter2016.Web.Controls.MessageControl" %>
<div class="row">
    <div class="col-md-2 alert alert alert-success" runat="server" id="User">
        <asp:Image ID="UserImage" runat="server" Width="100px" Visible="false" />
        <asp:Label ID="UserDate" runat="server" Text=""></asp:Label>
        <asp:Label ID="UserName" runat="server" Text=""></asp:Label>
    </div>
    <div class="col-md-8">
        <asp:Label ID="Text" CssClass="bubble" runat="server" Text=""></asp:Label>
    </div>
    <div class="col-md-2 alert alert-info" runat="server" id="Admin" Visible="False">
        <asp:Image ID="AdminImage" runat="server" Width="100px" Visible ="false" />
        <asp:Label ID="AdminDate" runat="server"></asp:Label>
        <asp:Label ID="AdminName" runat="server"></asp:Label>
    </div>
</div>