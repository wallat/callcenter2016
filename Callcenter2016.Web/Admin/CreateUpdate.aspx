﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateUpdate.aspx.cs" Inherits="Callcenter2016.Web.Admin.CreateUpdate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div ID="incidenceDiv" runat="server">
        <asp:Label ID="lblDate" runat="server" Text="Fecha de creación:" Visible="False"></asp:Label>
        <asp:Label ID="Date" runat="server" Visible="False"></asp:Label>
        <br />
        <asp:Label ID="lblEstado" runat="server" Text="Estado:" Visible="False"></asp:Label>
        <asp:Label ID="Estado" runat="server" Visible="False"></asp:Label>
        <br />
        <asp:Label ID="usertitle" runat="server" Text="Seleccione el usuario del que quiere crear la incidencia:"></asp:Label>
        <br />
        <asp:DropDownList ID="userList" runat="server"></asp:DropDownList>
        <br />
        <asp:Label ID="title" runat="server" Text="Introduzca el texto de la incidencia"></asp:Label>
        <br />
        <asp:TextBox ID="text" runat="server" Height="153px" TextMode="MultiLine" Width="500px"></asp:TextBox>
        <br />
        <asp:Label ID="titlee" runat="server" Text="Introduzca el nombre del equipo"></asp:Label>
        <br />
        <asp:TextBox ID="equipment" runat="server" Width="499px"></asp:TextBox>
        <br />
        <asp:Label ID="titlet" runat="server" Text="Seleccione tipo incidencia"></asp:Label>
        <br />
        <asp:DropDownList ID="type" runat="server">
            <asp:ListItem Value="0">Hardware</asp:ListItem>
            <asp:ListItem Value="1">Software</asp:ListItem>
            <asp:ListItem Value="2">Otros</asp:ListItem>
        </asp:DropDownList>
        <br />
        <asp:Label ID="titleP" runat="server" Text="Seleccione prioridad"></asp:Label>
        <br />
        <asp:DropDownList ID="priority" runat="server"/>            
        <br />
        Nota interna:<br />
        <asp:TextBox ID="note" runat="server" TextMode="MultiLine" Width="491px"></asp:TextBox>
        <br />
        <asp:HiddenField ID="Id" runat="server" />
        <br />
        <asp:Button ID="save" runat="server" OnClick="save_Click" Text="Guardar" />
        <br />
     </div>
    <asp:Label ID="result" runat="server"></asp:Label>
    <br />
    <br />
    
</asp:Content>
