﻿using CallCenter2016.Application;
using CallCenter2016.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using System.Data.Entity;

namespace Callcenter2016.Web.Admin
{
    public partial class Default : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        IncidenceManager incidenceManager = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            incidenceManager = new IncidenceManager(context);
            var incidences = incidenceManager
                            .GetAll()
                            .OrderBy(i=> i.Status).OrderBy(i=> i.Priority)
                            .Include(i=> i.Messages);
            string formatlink = "<a href='createupdate.aspx?Id={0}'>{1}</a>";
            foreach(var incidence in incidences)
            {
                var row = new TableRow();
                row.Cells.Add(new TableCell { Text = string.Format(formatlink, incidence.Id,incidence.CreatedDate.ToString("dd/MM/yyyy")) });
                row.Cells.Add(new TableCell { Text = string.Format(formatlink, incidence.Id,incidence.Messages.OrderBy(i=> i.Id).First().Text) });
                row.Cells.Add(new TableCell { Text = string.Format(formatlink, incidence.Id,incidence.Equipment) });
                row.Cells.Add(new TableCell { Text = string.Format(formatlink, incidence.Id,incidence.Status.ToString()) });
                row.Cells.Add(new TableCell { Text = string.Format(formatlink, incidence.Id,incidence.Messages.OrderByDescending(i=> i.Id).First().Date.ToString("dd/MM/yyyy")) });                
                tbody.Controls.Add(row);
            }
        }
    }
}