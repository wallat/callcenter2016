﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Callcenter2016.Web.Admin.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Admin/CreateUpdate.aspx" CssClass="btn btn-primary">Nueva incidencia</asp:HyperLink>
    <br /><br />
    <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Fecha</th>
                <th>Asunto</th>
                <th>Equipo</th>
                <th>Estado</th>
                <th>Ultima modificaci�n</th>                
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Fecha</th>
                <th>Asunto</th>
                <th>Equipo</th>
                <th>Estado</th>
                <th>Ultima modificaci�n</th>
            </tr>
        </tfoot>
        <tbody id="tbody" runat="server">            
        </tbody>
    </table>
     <%: Scripts.Render("~/bundles/datatables") %>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable({
                "oLanguage": { "sUrl": "//cdn.datatables.net/plug-ins/1.10.7/i18n/Spanish.json" }
            });
        } );

    </script>
</asp:Content>