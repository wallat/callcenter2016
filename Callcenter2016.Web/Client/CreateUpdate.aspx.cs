﻿using Callcenter2016.CORE;
using CallCenter2016.Application;
using CallCenter2016.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Callcenter2016.Web.Controls;
using System.Data.Entity;

namespace Callcenter2016.Web.Client
{
    public partial class CreateUpdate : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        IncidenceManager incidenceManager = null;
        MessageManager messageManager = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            incidenceManager = new IncidenceManager(context);
            messageManager = new MessageManager(context);
            
            
            var idtext = Request.QueryString["Id"];
            if(idtext!=null)
            {
                int id = 0;
                if(int.TryParse(idtext, out id))
                {
                    var incidence = incidenceManager.GetById(new object[] {id});
                    if(incidence!=null)
                    {
                        if(incidence.User_Id == User.Identity.GetUserId())
                        {
                            Id.Value = incidence.Id.ToString();
                            equipment.Text = incidence.Equipment;
                            equipment.Enabled = false;
                            type.SelectedValue = ((int)incidence.Type).ToString();
                            type.Enabled = false;                            

                            Date.Text = incidence.CreatedDate.ToString("dd/MM/yyyy");
                            Date.Visible = true;
                            lblDate.Visible = true;
                            Estado.Text = incidence.Status.ToString();
                            Estado.Visible = true;
                            lblEstado.Visible = true;

                            var messages = messageManager.GetAll()
                                            .Include(m=> m.User)
                                           .Where(m => m.Incidence_Id == incidence.Id)
                                           .OrderByDescending(m => m.Date);
                            var content = (ContentPlaceHolder)Master.FindControl("MainContent");
                            foreach(var message in messages)
                            {
                                var control = (MessageControl)Page.LoadControl("~/Controls/MessageControl.ascx");                               
                                control.Message = message;                                
                                content.Controls.Add(control);
                            }
                        }
                        else
                        {
                            //Todo: No es mio
                            result.Text = "No se ha encontrado la incidencia indicada";
                            incidenceDiv.Visible = false;
                        }
                    }
                    else
                    {
                        //TODO: error, no encontrado
                        result.Text = "No se ha encontrado la incidencia indicada";
                        incidenceDiv.Visible = false;
                    }
                }
                else
                {
                    //TODO: Error de parseo
                    result.Text = "No se ha encontrado la incidencia indicada";
                    incidenceDiv.Visible = false;
                }
            }
        }

        protected void save_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(Id.Value))
                {
                    #region insertar
                    Incidence incidence = new Incidence
                    {
                        CreatedDate = DateTime.Now,
                        Status = IncidenceStatus.Abierta,
                        Priority = IncidencePriority.Low,
                        Equipment = equipment.Text,
                        Type = (IncidenceType)Enum.Parse(typeof(IncidenceType), type.SelectedValue),
                        Messages = new List<Message>(),
                        User_Id = User.Identity.GetUserId()
                    };

                    incidence.Messages.Add(new Message
                    {
                        Date = DateTime.Now,
                        Text = text.Text,
                        User_Id = User.Identity.GetUserId()
                    });


                    incidenceManager.Add(incidence);
                    #endregion
                }
                else
                {
                    #region modificar
                    var message = new Message
                    {
                        Date = DateTime.Now,
                        Text = text.Text,
                        User_Id = User.Identity.GetUserId(),
                        Incidence_Id = int.Parse(Id.Value)
                    };
                    messageManager.Add(message);

                    var control = (MessageControl)Page.LoadControl("~/Controls/MessageControl.ascx");
                    control.Message = message;
                    var content = (ContentPlaceHolder)Master.FindControl("MainContent");
                    content.Controls.AddAt(5,control);
                    text.Text = "";
                    #endregion
                }
                incidenceManager.Context.SaveChanges();
                result.Text = "Incidencia guardada con exito.";
                result.CssClass = "has-success";
            }
            catch(Exception ex)
            {
                result.Text = "Se ha producido un error, si este persiste contacte con el administrador";
                result.CssClass = "has-error";
                //TODO: Guardar un log con el error
            }            
        }
    }
}