namespace CallCenter2016.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InternalNote : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Incidences", "InternalNote", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Incidences", "InternalNote");
        }
    }
}
