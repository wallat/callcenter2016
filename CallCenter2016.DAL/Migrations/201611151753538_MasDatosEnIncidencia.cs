namespace CallCenter2016.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MasDatosEnIncidencia : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Incidences", "Equipment", c => c.String());
            AddColumn("dbo.Incidences", "Type", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Incidences", "Type");
            DropColumn("dbo.Incidences", "Equipment");
        }
    }
}
