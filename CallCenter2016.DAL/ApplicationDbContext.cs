﻿using Callcenter2016.CORE;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallCenter2016.DAL
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        /// <summary>
        /// Colección persistible de incidencias
        /// </summary>
        public DbSet<Incidence> Incidences { get; set; }
        /// <summary>
        /// Colección persistible de mensajes
        /// </summary>
        public DbSet<Message> Messages { get; set; }
    }
}
