﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CallCenter2016.DAL;
using Callcenter2016.CORE;
using System.Linq;
using Moq;
using System.Collections.Generic;
using System.Data.Entity;

namespace CallCenter2016.Application.Test
{
    [TestClass]
    public class IncidenceManagerTest
    {
        [TestMethod]
        public void GetAll()
        {
            //Arrange
            //ApplicationDbContext context = new ApplicationDbContext();
            var IncidenceList = new List<Incidence>
            {
                new Incidence() { Id = 1, Type = IncidenceType.Hardware  },
                new Incidence() { Id = 2, Type = IncidenceType.Software  }
            }.AsQueryable();

            var mockIncidences = new Mock<DbSet<Incidence>>();
            mockIncidences.As<IQueryable<Incidence>>().Setup(m => m.Provider).Returns(IncidenceList.Provider);
            mockIncidences.As<IQueryable<Incidence>>().Setup(m => m.Expression).Returns(IncidenceList.Expression);
            mockIncidences.As<IQueryable<Incidence>>().Setup(m => m.ElementType).Returns(IncidenceList.ElementType);
            mockIncidences.As<IQueryable<Incidence>>().Setup(m => m.GetEnumerator()).Returns(IncidenceList.GetEnumerator);
            
            var mockDbContext = new Mock<ApplicationDbContext>();
            mockDbContext.Setup(e => e.Set<Incidence>()).Returns(mockIncidences.Object);
            IncidenceManager target = new IncidenceManager(mockDbContext.Object);
            //Act
            IQueryable<Incidence> result = target.GetAll();
            //Assert
            Assert.IsTrue(result.Any());
        }
    }
}
