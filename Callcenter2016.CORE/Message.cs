﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Callcenter2016.CORE
{
    /// <summary>
    /// Entidad de dominio de mensajes
    /// </summary>
    public class Message
    {
        /// <summary>
        /// Identificador del mensaje
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Texto del mensaje
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Fecha y Hora del mensaje
        /// </summary>
        public DateTime Date { get; set; }


        /// <summary>
        /// Identificador del usuario que ha escrito el mensaje
        /// </summary>
        [ForeignKey("User")]
        public string User_Id { get; set; }  

        /// <summary>
        /// Usuario que ha creado la mensaje
        /// </summary>
        public ApplicationUser User { get; set; }

        /// <summary>
        /// Identificador de la incidencia a la que pertenece
        /// </summary>
        [ForeignKey("Incidence")]
        public int Incidence_Id { get; set; }   

        /// <summary>
        /// Incidencia a la que pertenece
        /// </summary>
        public Incidence Incidence { get; set; }        
    }
}
