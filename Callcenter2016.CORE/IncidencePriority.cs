﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Callcenter2016.CORE
{
    /// <summary>
    /// Prioridad de la incidencia
    /// </summary>
    public enum IncidencePriority : int
    {
        /// <summary>
        /// Prioridad baja
        /// </summary>
        Low = 0,
        /// <summary>
        /// Prioridad media
        /// </summary>
        Medium = 1,
        /// <summary>
        /// Prioridad alta
        /// </summary>
        High = 2,
        SuperHigh = 3
    }
}
