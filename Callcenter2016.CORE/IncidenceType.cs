﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Callcenter2016.CORE
{
    /// <summary>
    /// Enumerado de tipo de incidencia
    /// </summary>
    public enum IncidenceType:int
    {
        Hardware = 0,
        Software = 1,
        Otros = 2
    }
}
