﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Callcenter2016.CORE
{
    /// <summary>
    /// Estados de una incidencia
    /// </summary>
    public enum IncidenceStatus : int
    {
        /// <summary>
        /// Incidencia abierta
        /// </summary>
        Abierta = 0,
        /// <summary>
        /// Incidencia en progreso
        /// </summary>
        EnProgreso = 1,
        /// <summary>
        /// Incidencia cerrada
        /// </summary>
        Cerrada = 2
    }
}
