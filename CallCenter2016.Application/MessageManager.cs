﻿using Callcenter2016.CORE;
using CallCenter2016.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallCenter2016.Application
{
    /// <summary>
    /// Manager de Message
    /// </summary>
    public class MessageManager : GenericManager<Message>
    {
        /// <summary>
        /// Constructor de la clase
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public MessageManager(ApplicationDbContext context)
            :base(context)
        {

        }

        
    }
}
