﻿using CallCenter2016.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallCenter2016.Application
{
    /// <summary>
    /// Clase Manager generica 
    /// </summary>
    public class GenericManager<T>
        where T: class
    {
        /// <summary>
        /// Contexto de datos
        /// </summary>
        public ApplicationDbContext Context { get; private set; }

        /// <summary>
        /// Constructor de la clase generica
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public GenericManager(ApplicationDbContext context)
        {
            Context = context;
        }

        /// <summary>
        /// Añade una entidad al contexto
        /// </summary>
        /// <param name="entity">Entidad a añadir</param>
        /// <returns>Entidad añadida</returns>
        public T Add(T entity)
        {            
            return Context.Set<T>().Add(entity);
        }

        /// <summary>
        /// Modificar una entidad
        /// </summary>
        /// <param name="id">identificador del objeto a modificar</param>
        /// <param name="entity">Nuevos valores</param>
        /// <returns></returns>
        public T Update (object[] id, T entity)
        {
            T original = GetById(id);
            if (original != null)
                Context.Entry<T>(original).CurrentValues.SetValues(entity);
            return entity;
        }

        /// <summary>
        /// Elimina una entidad del contexto
        /// </summary>
        /// <param name="entity">Entidad a eliminar</param>
        /// <returns>Entidad eliminada</returns>
        public T Remove(T entity)
        {
            return Context.Set<T>().Remove(entity);
        }

        /// <summary>
        /// Obtiene un elemento por sus claves
        /// </summary>
        /// <param name="keys">Claves del elemento</param>
        /// <returns>Entidad encontrada</returns>
        public T GetById(object[] keys)
        {
            return Context.Set<T>().Find(keys);
        }

        /// <summary>
        /// Obtiene un objeto por su identificador
        /// </summary>
        /// <param name="id">Identificador</param>
        /// <returns>Entidad</returns>
        public T GetById(int id)
        {
            return GetById(new object[] { id });
        }

        /// <summary>
        /// Obtiene un objeto por su identificador
        /// </summary>
        /// <param name="id">Identificador</param>
        /// <returns>Entidad</returns>
        public T GetById(string id)
        {
            return GetById(new object[] { id });
        }

        /// <summary>
        /// Obtiene todos los elementos de una entidad
        /// </summary>
        /// <returns>IQueriable de los elementos</returns>
        public IQueryable<T> GetAll()
        {
            return Context.Set<T>();
        }

    }
}
