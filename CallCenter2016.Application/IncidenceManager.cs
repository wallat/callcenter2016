﻿using Callcenter2016.CORE;
using CallCenter2016.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallCenter2016.Application
{
    /// <summary>
    /// Manager de incidencias
    /// </summary>
    public class IncidenceManager : GenericManager<Incidence>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public IncidenceManager(ApplicationDbContext context)
            :base(context)
        {

        }

        /// <summary>
        /// Obtiene todas las incidencias de un usuario
        /// </summary>
        /// <param name="userId">Identificador de usuario</param>
        /// <returns>Incidencias del usuario</returns>
        public IQueryable<Incidence> GetByUser(string userId)
        {
            return Context.Incidences.Where(e => e.User_Id == userId);
        }

        
    }
}
